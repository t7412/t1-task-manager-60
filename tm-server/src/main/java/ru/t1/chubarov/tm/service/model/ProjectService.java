package ru.t1.chubarov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.repository.model.IProjectModelRepository;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.model.IProjectService;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.DescriptionEmptyException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.NameEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectModelRepository repository;

    @NotNull
    @Autowired
    private IUserModelRepository userRepository;

    public User findOneById(@Nullable final String userId) {
        return userRepository.findOneById(userId);
    }

    @Override
    @Transactional
    public void create(@Nullable final String userId, @NotNull final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project(name, Status.NOT_STARTED.toString());
        project.setName(name);
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        project.setUser(user);
        repository.add(project);
    }

    @Override
    @Transactional
    public void create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project(name, Status.NOT_STARTED.toString());
        project.setName(name);
        project.setDescription(description);
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        project.setUser(user);
        repository.add(project);
    }

    @Override
    @Transactional
    public void add(@Nullable final Project model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        repository.add(model);
    }

    @Override
    @Transactional
    public void addByUserId(@Nullable final String userId, @Nullable final Project model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        model.setUser(user);
        repository.add(model);
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @NotNull final String name, @NotNull final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        @NotNull final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status.toString());
        repository.update(project);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUser(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public Project findOneById(@NotNull String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project model = repository.findOneByIdByUser(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final Project model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
    }


    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project model = new Project();
        repository.removeOneById(userId, id);
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

    @Override
    public Long getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSizeByUser(userId);
    }

    @Override
    public void addAll(@NotNull final Collection<Project> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        for (@NotNull final Project project : models) {
            add(project);
        }
    }

    @Override
    public void clear() {
            repository.clear();
    }

}