package ru.t1.chubarov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.repository.model.ISessionModelRepository;
import ru.t1.chubarov.tm.api.service.model.ISessionService;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.SessionNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.model.Session;

import java.util.Collection;
import java.util.List;

@Service
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private ISessionModelRepository repository;

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.add(model);
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<Session> findAll(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUser(userId);
    }

    @NotNull
    @Override
    public Session findOneById(@NotNull String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session model = repository.findOneById(id);
        if (model == null) throw new SessionNotFoundException();
        return model;
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.removeOneById(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeAll(@NotNull final String userId, @Nullable final Collection<Session> models) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        for (@NotNull final Session session : models) {
            repository.removeOneById(userId, session.getId());
        }
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session model = new Session();
        model.setId(id);
        repository.removeOneById(userId, id);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session model = new Session();
        model.setId(id);
        repository.remove(model);
    }

    @Override
    public long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSizeByUser(userId);
    }

    @Override
    public long getSize() throws Exception {
        return repository.getSize();
    }

}