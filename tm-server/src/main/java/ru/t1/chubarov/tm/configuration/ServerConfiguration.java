package ru.t1.chubarov.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.chubarov.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseUsername());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.chubarov.tm.model", "ru.t1.chubarov.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2DDL_Auto());
        properties.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDatabaseShowSql());

//        properties.put(org.hibernate.cfg.Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseSecond());
//        properties.put(org.hibernate.cfg.Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUseQueryCache());
//        properties.put(org.hibernate.cfg.Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseMinimalPuts());
//        properties.put(org.hibernate.cfg.Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseRegionPrefix());
//        properties.put(org.hibernate.cfg.Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseRegionFactory());
//        properties.put(org.hibernate.cfg.Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseProviderConfig());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
