package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserOwnerModel extends AbstractModel {

    @Nullable
    @Column(name = "user_id")
    private String userId;

}
