package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Project;

import java.util.List;

public interface IProjectModelRepository extends IModelRepository<Project>{

    @Nullable
    List<Project> findAllByUser(@Nullable String userId);

    @Nullable
    Project findOneByIdByUser(@Nullable String userId, @Nullable String id);

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable String userId, @NotNull Project model);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    long getSizeByUser(@Nullable String userId);

}
