package ru.t1.chubarov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.listener.AbstractListener;


import java.util.Collection;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = commandService.getTerminalCommands();
        for (@Nullable final AbstractListener command : commands) System.out.println(command);
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show help program.";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

}
