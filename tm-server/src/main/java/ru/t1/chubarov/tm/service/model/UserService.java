package ru.t1.chubarov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.model.IProjectService;
import ru.t1.chubarov.tm.api.service.model.ITaskService;
import ru.t1.chubarov.tm.api.service.model.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IUserModelRepository repository;

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL.toString());
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException(login, email);
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL.toString());
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role.toString());
        repository.add(user);
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public User findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User model = repository.findOneById(id);
        if (model == null) throw new UserNotFoundException();
        return model;
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final User model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        @Nullable final String userId = model.getId();
        repository.remove(model);
        taskService.removeAll(userId);
        projectService.removeAll(userId);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        removeOne(user);
    }

    @Override
    @Transactional
    public void removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException("login", email);
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        removeOne(user);
    }

    @Override
    @Transactional
    public void setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.update(user);
    }

    @Override
    @Transactional
    public void updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findOneById(id);
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        repository.update(user);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.update(user);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<User> set(@NotNull Collection<User> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        clear();
        for (@NotNull final User user : models) {
            repository.add(user);
        }
        return models;
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

}