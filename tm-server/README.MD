# TASK MANAGER

## DEVELOPER INFO

**NAME**: Evgeni Chubarov

**EMAIL**: echubarov@t1-consulting.ru

**EMAIL**: evgeni.chubarov@yandex.ru

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: i7

**RAM**: 16GB

**SSD**: 256GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./t1-task-manager.jar
```
